(* ::Package:: *)

$PrePrint=TraditionalForm;
directory=If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName;
Get[FileNameJoin[{directory,"..","..","calcloop","CalcLoop.wl"}]];
SetOptions[FeynArtsReadAmp,"ExternalMomentumName"->(Symbol["p"<>ToString@#]&)];


(* ::Section::Closed:: *)
(*1\:3001Generate Feynman amplitudes using FeynArts*)


(* ::Subsection::Closed:: *)
(*1 FeynArtsCreateAmp*)


processInf=Association[
	"Process"->"e+ e- -> \[Mu]+ \[Mu]- 0 0",
	"Options"-><|"InsertFields"->"{Model -> \"QED\"}","Paint"->"AutoEdit->False"|>
];
FeynArtsCreateAmp[FileNameJoin[{directory,"amplitudes","1mumu"}],processInf]//CLTiming


processInf=Association[
	"Process"->"e+ e- -> u u~ g 0 0",
	"Options"-><|"InsertFields"->"{ExcludeParticles\[Rule]{_S*(_:1),V[2|3|4]*(_:1)}}"|>
];
FeynArtsCreateAmp[FileNameJoin[{directory,"amplitudes","2uu"}],processInf]//CLTiming


processInf=Association[
	"Process"->"e+ e- -> H Z 0 0",
	"Options"-><|"InsertFields"->
		"{ExcludeFieldPoints -> {FieldPoint[0][-F[1|2, {_}], F[1|2, {_}], _S*(_:1)]}}",
		"CreateTopologies"->"{}"|>
];
FeynArtsCreateAmp[FileNameJoin[{directory,"amplitudes","3HZ"}],processInf]//CLTiming


processInf=Association[
	"Process"->"\[Gamma] \[Gamma] -> t t~ 0 0",
	"Options"-><|"InsertFields"->"{ExcludeParticles\[Rule]{_S*(_:1),V[2|3|4]*(_:1)}}"|>
];
FeynArtsCreateAmp[FileNameJoin[{directory,"amplitudes","4rr"}],processInf]//CLTiming


processInf=Association[
	"Process"->"g g -> t t~ 0 0",
	"Options"-><|"CreateFeynAmp"->"{GaugeRules\[Rule]{}}","InsertFields"->
		"{ExcludeFieldPoints -> {FieldPoint[0][-F[4, {_}], F[4, {_}], _S*(_:1)],\
          FieldPoint[0][-F[3, {1|2}], F[3, {1|2}], _S*(_:1)]}}"|>
];
FeynArtsCreateAmp[FileNameJoin[{directory,"amplitudes","5gg"}],processInf]//CLTiming


processInf=Association[
	"Process"->"ug ug~ -> t t~ 0 0",
	"Options"-><|"InsertFields"->"{ExcludeFieldPoints -> {}}"|>
];
FeynArtsCreateAmp[FileNameJoin[{directory,"amplitudes","6ugug"}],processInf]//CLTiming


processInf=Association[
	"Process"->"g g -> H H 1 0",
	"Options"-><|"InsertFields"->"{ExcludeFieldPoints -> {\
			FieldPoint[0][-F[4, {_}], F[4, {_}], _S*(_:1)],\
			FieldPoint[0][-F[3, {1|2}], F[3, {1|2}], _S*(_:1)]}}"|>
];
FeynArtsCreateAmp[FileNameJoin[{directory,"amplitudes","7HH"}],processInf]//CLTiming


processInf=Association[
	"Process"->"g g -> t t~ g 0 0",
	"Options"-><|"CreateFeynAmp"->"{GaugeRules\[Rule](g_GaugeXi:>g)}","InsertFields"->
		"{ExcludeFieldPoints -> {FieldPoint[0][-F[4, {_}], F[4, {_}], _S*(_:1)],\
			FieldPoint[0][-F[3, {1|2}], F[3, {1|2}], _S*(_:1)]}}"|>
];
FeynArtsCreateAmp[FileNameJoin[{directory,"amplitudes","8ggg"}],processInf]//CLTiming


processInf=Association[
	"Process"->"e+ e- -> \[Mu]+ \[Mu]- 1 0",
	"Options"-><|"InsertFields"->"{Model -> \"QED\"}",
		"Paint"->"AutoEdit->False"|>
];
FeynArtsCreateAmp[FileNameJoin[{directory,"amplitudes","91mumu"}],processInf]//CLTiming


processInf=Association[
	"Process"->"e+ e- -> \[Mu]+ \[Mu]- 2 0",
	"Options"-><|"InsertFields"->"{Model -> \"QED\"}",
		"Paint"->"AutoEdit->False"|>
];
FeynArtsCreateAmp[FileNameJoin[{directory,"amplitudes","92mumu"}],processInf]//CLTiming


(* ::Subsection::Closed:: *)
(*2 FeynArtsReadAmp*)


amps=FileNames["*.FAs",directory,Infinity]
amps//Length


{head,amp}=FeynArtsReadAmp[amps[[5]],"ExternalMomentumName"->(Symbol["p"<>ToString@#]&)]
amp=amp/.a_(b_+1/2 c_)+a_(b_-1/2 c_):>2a*b/.CalcLoopSymbol["GaugeXi"][_]:>1


(* ::Section::Closed:: *)
(*2\:3001Simplify amplitudes: introduction*)


(* ::Subsection::Closed:: *)
(*1 Color matrix*)


{SUNT[i,j],SUNT[i,a,j],SUNT[0,a,0]}//CLForm
%//SUNSimplify


SUNT[i,a,j]SUNT[k,a,l]//CLForm
%//SUNSimplify//Expand


SUNT[i,a,a,j]//CLForm
%//SUNSimplify


SUNT[0,a,b,c,a,d,e,0]SUNT[0,a1,c,d,a1,b,e,0]//CLForm
%//SUNSimplify


SUNT[i,a,b,c,f,d,e,j]SUNT[j,f,c,d,a,b,e,k]//CLForm
%//SUNSimplify


SUNF[a,b,c]//CLForm
%//SUNSimplify


SUNF[a,b,c]SUNF[a,b,d]//CLForm
%//SUNSimplify


SUNF[a,b,c]SUNT[0,a,b,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,0]*
	SUNT[0,c,a1,a2,a3,a4,a5,a6,a7,a8,a9,a10,a11,a12,0]//CLForm
%//SUNSimplify//CLTiming


(* ::Subsection::Closed:: *)
(*2 Dirac matrix	*)


GA[mu,mu]//CLForm
%//DiracSimplify
%%//StandardForm


?DiracChain


GA[mu,nu1,mu]//CLForm
%//DiracSimplify


GA[mu,nu1,nu2,mu]//CLForm
%//DiracSimplify
%//StandardForm


DiracChain[GA[mu],GS[p,q,p],GA[mu]]//CLForm
%//DiracSimplify
%%//StandardForm


GS[p,q,p]//CLForm
%//DiracSimplify


GA[mu,nu1,nu2,nu3,mu]//CLForm
DiracSimplify[%,"OrderQ"->True]//Simplify


GA[mu,nu1,nu2,nu3,nu4,mu]//CLForm
%//DiracSimplify


GA[mu,nu1,nu2,nu3,nu4,nu5,mu]//CLForm
%//DiracSimplify//CLTiming


DiracChain[DiracSpinor[{p+q,1},m1],CLForm@GS[p,q],DiracSpinor[{p-q,1},m2]]
DiracSimplify[%]//SimplifyList[#,_DiracChain]&


DiracChain[DiracSpinor[{p+q,1},m1],CLForm@GS[r,p,q],DiracSpinor[{t,1},m2]]
DiracSimplify[%]//SimplifyList[#,_DiracChain]&
DiracSimplify[%%,"OrderQ"->True]//SimplifyList[#,_DiracChain]&


DiracChain[0,CLForm@GS[p,q,r2,r1],0]
DiracSimplify[%]


(* ::Subsection::Closed:: *)
(*3 Lorentz indexes*)


(LV[p,mu1]LV[q,mu2]MT[mu3,mu4]+LV[p,mu3]LV[q,mu1]MT[mu2,mu4])^2//CLForm
%//LorentzSimplify//CLTiming


(* ::Subsection::Closed:: *)
(*4 GeneralizedApart*)


(* ::Subsubsection::Closed:: *)
(*simple examples*)


(*The same as Apart for univariate problems*)
GeneralizedApart[(x^5-2)/((1+x+x^2)(2+x)(1-x))]
Apart[(x^5-2)/((1+x+x^2)(2+x)(1-x))]


(*Different for multivariate problems*)
res1=GeneralizedApart[(x^3-y)/(x(x+y)(x-y))]
res2=Apart[(x^3-y)/(x(x+y)(x-y))]
res3=Apart[(x^3-y)/(x(x+y)(x-y)),x]
res4=Apart[(x^3-y)/(x(x+y)(x-y)),y]
{res1,res2,res3,res4}//Factor


(*Unique result: independent of the input form*)
(*"CancelQ"->True: cancel spurious poles as far as possible*)
exp={(2y-z)/(y(y+z)(y-z)),1/(y(y+z))+1/(2z(y-z))-1/(2z(y+z))}
GeneralizedApart/@exp
GeneralizedApart[exp]
GeneralizedApart[exp,"CancelQ"->True]


(* ::Subsubsection::Closed:: *)
(*a lengthy example*)


{exp,vars}={{(y^2*t*((3*(-4 + d)*(-1 - y + z)*MI[db, 1, 1])/2 + 
    (-4 + d)*(-3 - (7*(-x + y))/2 - 3*(-1 - y + z) - t)*
     MI[db, 1, 1] + ((-4 + d)*(x - (-x + y)^2/2 - 
       ((-x + y)*(-1 - y + z))/2)*t*MI[db, 1, 1])/x + 
    ((5*(-x + y))/2 + (3*(1 + y - 2*(-x + y) - z))/
       2 + (3*(-1 - x + z))/2 + d*(-3 - (7*(-x + y))/2 - 
        3*(-1 - y + z) - t) + 2*t)*MI[db, 1, 1] + 
    ((-4 + d)*(-x + y)*(4*x + (-x + y)*t + 
       (-1 - y + z)*t)*MI[db, 1, 1])/(2*x) + 
    (-((-6*x + (-3 - (7*(-x + y))/2 - 3*(-1 - y + z) - 
           t)*t + (3*(4*x + (-x + y)*t + 
            (-1 - y + z)*t))/2)/t) + 
      (2*(3*x*(-y + z) + (x + y)*
          (-3 - (7*(-x + y))/2 - 3*(-1 - y + z) - t) + 
         (x - (-x + y)^2/2 - ((-x + y)*(-1 - y + 
              z))/2)*(3 + 2*t) + (-x + y)*
          (4*x + (-x + y)*t + (-1 - y + z)*t)))/
       y - (3*x*(-y + z) + (-1 + x + z)*
         (-3 - (7*(-x + y))/2 - 3*(-1 - y + z) - t) + 
        (x - (-x + y)^2/2 - ((-x + y)*(-1 - y + 
             z))/2)*(-1 + 2*t) + (1 - x + y)*
         (4*x + (-x + y)*t + (-1 - y + z)*t))/
       z - (2*(3*x*(-1 - y + (1 + x - z)/2 + z) + 
         (2*x + ((-x + y)*(1 + x - z))/2 + 
           ((1 + x - z)*(-1 - y + z))/2)*
          (-3 - (7*(-x + y))/2 - 3*(-1 - y + z) - t) + 
         (x - (-x + y)^2/2 - ((-x + y)*(-1 - y + 
              z))/2)*((-1 - x + z)/2 + 2*t) + 
         (-x + y + (1 + x - z)/2)*(4*x + 
           (-x + y)*t + (-1 - y + z)*t)))/
       (x - (-x + y)^2/4 - ((-x + y)*
          (-1 - y + z))/2 - (-1 - y + z)^2/4))*MI[db, 1, 1]))/
  (z*(x - (-x + y)^2/4 - 
     ((-x + y)*(-1 - y + z))/2 - (-1 - y + z)^2/4)^2*
   (x + 2*(-x + y)*t + 2*(-1 - y + z)*t + 
     4*t^2)^2), 
 (y^2*t*(-2*(-4 + d)*(-x + y)*MI[db, 1, 1] + 
    (-4 + d)*(4 + 4*(-x + y) + (7*(-1 - y + z))/2 - t)*
     MI[db, 1, 1] + 
    ((-4 + d)*(x - ((-x + y)*(-1 - y + z))/2 - 
       (-1 - y + z)^2/2)*t*MI[db, 1, 1])/x + 
    (1 + y - z - 2*(-1 - x + z) - 
      2*(x - y - 2*(-1 - y + z)) + 
      d*(4 + 4*(-x + y) + (7*(-1 - y + z))/2 - t) + 
      2*t)*MI[db, 1, 1] + ((-4 + d)*(-1 - y + z)*
      (-3*x + (-x + y)*t + (-1 - y + z)*t)*
      MI[db, 1, 1])/(2*x) + 
    (-((-6*x + (4 + 4*(-x + y) + (7*(-1 - y + z))/2 - 
           t)*t - 2*(-3*x + (-x + y)*t + 
           (-1 - y + z)*t))/t) - 
      (-4*x*(1 - x + y) + (-1 + x + z)*
         (4 + 4*(-x + y) + (7*(-1 - y + z))/2 - t) + 
        (x - ((-x + y)*(-1 - y + z))/2 - 
          (-1 - y + z)^2/2)*(-1 + 2*t) + (-y + z)*
         (-3*x + (-x + y)*t + (-1 - y + z)*t))/
       z + (2*(-4*x*(-x + y) + (x + y)*
          (4 + 4*(-x + y) + (7*(-1 - y + z))/2 - t) + 
         (x - ((-x + y)*(-1 - y + z))/2 - 
           (-1 - y + z)^2/2)*(3 + 2*t) + (-y + z)*
          (-3*x + (-x + y)*t + (-1 - y + z)*t)))/
       y - (2*(-4*x*(-x + y + (1 + x - z)/2) + 
         (2*x + ((-x + y)*(1 + x - z))/2 + 
           ((1 + x - z)*(-1 - y + z))/2)*
          (4 + 4*(-x + y) + (7*(-1 - y + z))/2 - t) + 
         (x - ((-x + y)*(-1 - y + z))/2 - 
           (-1 - y + z)^2/2)*((-1 - x + z)/2 + 2*t) + 
         (-1 - y + (1 + x - z)/2 + z)*(-3*x + 
           (-x + y)*t + (-1 - y + z)*t)))/
       (x - (-x + y)^2/4 - ((-x + y)*
          (-1 - y + z))/2 - (-1 - y + z)^2/4))*MI[db, 1, 1]))/
  (z*(x - (-x + y)^2/4 - 
     ((-x + y)*(-1 - y + z))/2 - (-1 - y + z)^2/4)^2*
   (x + 2*(-x + y)*t + 2*(-1 - y + z)*t + 
     4*t^2)^2), 
 (y^2*t*(((-4 + d)*(-1 - y + z)*
      ((3*x*(-x + y))/2 + (3*x*(-1 - y + z))/2)*
      MI[db, 1, 1])/(2*x) + ((-4 + d)*(-1/2*(x*(-x + y)) + 
       (x*(-1 - y + z))/2)*t*MI[db, 1, 1])/x + 
    ((-4 + d)*(-x + y)*(2*x*(-x + y) + 
       2*x*(-1 - y + z) + 2*x*t)*MI[db, 1, 1])/
     (2*x) + (-4 + d)*(-2*(-x + y) - 2*(-x + y)^2 - 
      (3*(-1 - y + z))/2 - (7*(-x + y)*(-1 - y + z))/
       2 - (3*(-1 - y + z)^2)/2 - t - (-x + y)*t - 
      (-1 - y + z)*t)*MI[db, 1, 1] + 
    (3*x + (-1 - y + z)*((3*(-x + y))/2 + 
        (3*(-1 - y + z))/2) + 
      ((-x + y)*(-4 - 8*(-x + y) - 7*(-1 - y + z) - 
         2*t))/2 + ((-1 - y + z)*(-3 - 7*(-x + y) - 
         6*(-1 - y + z) - 2*t))/2 + (x - z)*t + 
      2*((x - y)/2 + (-1 - y + z)/2)*t + 
      (-x + y)*(2*(-x + y) + 2*(-1 - y + z) + 
        2*t) + d*(-2*(-x + y) - 2*(-x + y)^2 - 
        (3*(-1 - y + z))/2 - (7*(-x + y)*
          (-1 - y + z))/2 - (3*(-1 - y + z)^2)/2 - t - 
        (-x + y)*t - (-1 - y + z)*t))*MI[db, 1, 1] + 
    ((2*((-y + z)*((3*x*(-x + y))/2 + 
           (3*x*(-1 - y + z))/2) + 
         (-1/2*(x*(-x + y)) + (x*(-1 - y + z))/2)*
          (3 + 2*t) + (-x + y)*(2*x*(-x + y) + 
           2*x*(-1 - y + z) + 2*x*t) + 
         (x + y)*(-2*(-x + y) - 2*(-x + y)^2 - 
           (3*(-1 - y + z))/2 - (7*(-x + y)*
             (-1 - y + z))/2 - (3*(-1 - y + z)^2)/2 - t - 
           (-x + y)*t - (-1 - y + z)*t)))/y - 
      ((-y + z)*((3*x*(-x + y))/2 + 
          (3*x*(-1 - y + z))/2) + 
        (-1/2*(x*(-x + y)) + (x*(-1 - y + z))/2)*
         (-1 + 2*t) + (1 - x + y)*(2*x*(-x + y) + 
          2*x*(-1 - y + z) + 2*x*t) + 
        (-1 + x + z)*(-2*(-x + y) - 2*(-x + y)^2 - 
          (3*(-1 - y + z))/2 - (7*(-x + y)*
            (-1 - y + z))/2 - (3*(-1 - y + z)^2)/2 - t - 
          (-x + y)*t - (-1 - y + z)*t))/z - 
      (2*((-1 - y + (1 + x - z)/2 + z)*
          ((3*x*(-x + y))/2 + (3*x*(-1 - y + z))/2) + 
         (-1/2*(x*(-x + y)) + (x*(-1 - y + z))/2)*
          ((-1 - x + z)/2 + 2*t) + 
         (-x + y + (1 + x - z)/2)*(2*x*(-x + y) + 
           2*x*(-1 - y + z) + 2*x*t) + 
         (2*x + ((-x + y)*(1 + x - z))/2 + 
           ((1 + x - z)*(-1 - y + z))/2)*
          (-2*(-x + y) - 2*(-x + y)^2 - 
           (3*(-1 - y + z))/2 - (7*(-x + y)*
             (-1 - y + z))/2 - (3*(-1 - y + z)^2)/2 - t - 
           (-x + y)*t - (-1 - y + z)*t)))/
       (x - (-x + y)^2/4 - ((-x + y)*
          (-1 - y + z))/2 - (-1 - y + z)^2/4) - 
      (-2*((3*x*(-x + y))/2 + (3*x*(-1 - y + z))/2) + 
        (3*(2*x*(-x + y) + 2*x*(-1 - y + z) + 
           2*x*t))/2 + t*(-2*(-x + y) - 
          2*(-x + y)^2 - (3*(-1 - y + z))/2 - 
          (7*(-x + y)*(-1 - y + z))/2 - 
          (3*(-1 - y + z)^2)/2 - t - (-x + y)*t - 
          (-1 - y + z)*t))/t)*MI[db, 1, 1]))/
  (z*(x - (-x + y)^2/4 - 
     ((-x + y)*(-1 - y + z))/2 - (-1 - y + z)^2/4)^2*
   (x + 2*(-x + y)*t + 2*(-1 - y + z)*t + 
     4*t^2)^2)},{t,x,y,z}};


(*Many singularities*)
Times@@Cases[exp,_^n_/;n<0,Infinity]//Factor


GeneralizedApart[exp,vars]//CLTiming;
Times@@Cases[%,_^n_/;n<0,Infinity]//Factor


(*Slower if try to cancel each pole*)
GeneralizedApart[exp,vars,"CancelQ"->True]//CLTiming;
Times@@Cases[%,_^n_/;n<0,Infinity]//Factor


(* ::Subsection::Closed:: *)
(*5 Simplify amplitudes: step-by-step*)


amps=FileNames["*.FAs",directory,Infinity]
amps//Length


{head,amp}=FeynArtsReadAmp[amps[[5]],"ExternalMomentumName"->(Symbol["p"<>ToString@#]&)]
amp=amp/.a_(b_+1/2 c_)+a_(b_-1/2 c_):>2a*b/.CalcLoopSymbol["GaugeXi"][_]:>1


GenerateScalarProducts[head]//InputForm


all=Flatten@Outer[Times,amp,ComplexConjugate[amp]]


sq=FermionSpinSum[all,"Helicity"->All]
sq=BosonSpinSum[sq,"Helicity"->All]


sq1=sq//SUNSimplify//CLTiming


sq2=sq1//LorentzSimplify//CLTiming


sq3=sq2//DiracSimplify//CLTiming


sq4=sq3/.SUNN->3/.CalcLoopSymbol["GS"]->1//MomentumSimplify;
sq4=Plus@@sq4//Simplify


GeneralizedApart[sq4,{SP[p1,p2],SP[p1,p3]},"Factoring"->Simplify,"DenominatorFunction"->{GPD,1/GPDExplicit@#&}]


(* ::Section::Closed:: *)
(*3\:3001Simplify amplitudes: automatic*)


amps=FileNames["*.FAs",directory,Infinity]
amps//Length


(* ::Subsection::Closed:: *)
(*2.1 \[Gamma] \[Gamma] -> t tbar*)


proNo=4;
dir=FileNameJoin[{directory,"squaredAmp","4rr"}];


(*Using -g^\[Mu]\[Nu] for photon polarization sum*)
sqBorn=AmplitudeSquared[dir,amps[[proNo]],amps[[proNo]],"ReadSaved"->False,
	"Helicity"->All,"ReferenceMomentum"->{}];
res=Plus@@Get[sqBorn]["Expression"]//GPDExplicit//Simplify


(*Using physical polarization*)
sqBornPhy=AmplitudeSquared[dir,amps[[proNo]],amps[[proNo]],"ReadSaved"->False,
	"Helicity"->All,"ReferenceMomentum"->{p1->p2,p2->p1}];
resPhy=Plus@@Get[sqBornPhy]["Expression"]//GPDExplicit//Simplify


(*0*)
res-resPhy


(*helicity=1 for the first photon*)
sqBornP=AmplitudeSquared[dir,amps[[proNo]],amps[[proNo]],"ReadSaved"->False,
	"Helicity"->{p1->1},"ReferenceMomentum"->{p1->n}];


resP=Plus@@Get[sqBornP]["Expression"]//GPDExplicit//Simplify


(*helicity=-1 for the first photon*)
sqBornM=AmplitudeSquared[dir,amps[[proNo]],amps[[proNo]],"ReadSaved"->False,
	"Helicity"->{p1->-1},"ReferenceMomentum"->{p1->n}];
resM=Plus@@Get[sqBornM]["Expression"]//GPDExplicit//Simplify


(*{1/2,1/2}*)
{resP,resM}/res


(* ::Subsection::Closed:: *)
(*2.2 g g ->  t tbar*)


proNo=5;
dir=FileNameJoin[{directory,"squaredAmp","5gg"}];


(*Using -g^\[Mu]\[Nu] for gluon polarization sum*)
sqBornFeynman=AmplitudeSquared[dir,amps[[proNo]],amps[[proNo]],"ReadSaved"->False,
	"Helicity"->All,"ReferenceMomentum"->{}];
resFeynman=Plus@@Get[sqBornFeynman]["Expression"]//GPDExplicit//Simplify


(*Using physical polarization*)
sqBornPhy=AmplitudeSquared[dir,amps[[proNo]],amps[[proNo]],"ReadSaved"->False,
	"Helicity"->All,"ReferenceMomentum"->{p1->p2,p2->p1}];
resPhy=Plus@@Get[sqBornPhy]["Expression"]//GPDExplicit//Simplify


(*Nonzero*)
resPhy-resFeynman//Simplify


(*Different reference momentum*)
sqBornPhy1=AmplitudeSquared[dir,amps[[proNo]],amps[[proNo]],"ReadSaved"->False,
	"Helicity"->All,"ReferenceMomentum"->n];
resPhy1=Plus@@Get[sqBornPhy1]["Expression"]//GPDExplicit//Simplify


(*0*)
resPhy-resPhy1


(*Using R\[Xi] gauge with symbolic \[Xi]*)
sqBornXi=AmplitudeSquared[dir,amps[[proNo]],amps[[proNo]],"ReadSaved"->False,
	"Helicity"->All,"ReferenceMomentum"->{},"GaugeXi"->"GaugeXi"[_]->\[Xi]];
resXi=Plus@@Get[sqBornXi]["Expression"]//GPDExplicit//Simplify


(*0*)
resFeynman-resXi


(*helicity=1 for the first gluon*)
sqBornP=AmplitudeSquared[dir,amps[[proNo]],amps[[proNo]],"ReadSaved"->False,
	"Helicity"->{p1->1},"ReferenceMomentum"->{p1->p2}];
resP=Plus@@Get[sqBornP]["Expression"]//GPDExplicit//Simplify


(*helicity=-1 for the first gluon*)
sqBornM=AmplitudeSquared[dir,amps[[proNo]],amps[[proNo]],"ReadSaved"->False,
	"Helicity"->{p1->-1},"ReferenceMomentum"->{p1->p2}];
resM=Plus@@Get[sqBornM]["Expression"]//GPDExplicit//Simplify


(*0*)
resPhy-(resP+resM)//Simplify


(*Ghost contribution*)
sqBornUg=AmplitudeSquared[dir,amps[[proNo+1]],amps[[proNo+1]],"ReadSaved"->False,
	"Helicity"->All,"ReferenceMomentum"->{}];
resUg=Plus@@Get[sqBornUg]["Expression"]//GPDExplicit//Simplify


(*0*)
(*+2: a (-1) factor has been included in AmplitudeSquared*)
resFeynman+2resUg-resPhy//Simplify


(* ::Subsection::Closed:: *)
(*2.3 g g ->  t tbar - WTI*)


proNo=5;
dir=FileNameJoin[{directory,"squaredAmp","5gg"}];


(*Replace the polarization of a gluon by its momentum*)
projectEps1:=#/.PolarizationVector[{p1,1},LorentzIndex["mu"[1]]]->CLForm@LV[p1,"mu"[1]]&
projectEps12:=#/.{PolarizationVector[{p1,1},LorentzIndex["mu"[1]]]->CLForm@LV[p1,"mu"[1]],
	PolarizationVector[{p2,1},LorentzIndex["mu"[2]]]->CLForm@LV[p2,"mu"[2]]}&


(*p1_\[Mu] M^\[Mu]\[Nu]!=0*)
sqBornFeynman=AmplitudeSquared[dir,amps[[proNo]],1,"Operation"->{projectEps1,Identity,Identity},
	"ReadSaved"->False,"Parallelize"->False,"Helicity"->All,"ReferenceMomentum"->{}];
resFeynman=Plus@@Get[sqBornFeynman]["Expression"]//GPDExplicit//Simplify


(*p1_\[Mu] p2_\[Nu] M^\[Mu]\[Nu]=0*)
sqBornFeynman=AmplitudeSquared[dir,amps[[proNo]],1,"Operation"->{projectEps12,Identity,Identity},
	"ReadSaved"->False,"Parallelize"->False,"Helicity"->All,"ReferenceMomentum"->{}];
resFeynman=Plus@@Get[sqBornFeynman]["Expression"]//GPDExplicit//Simplify


(* ::Subsection::Closed:: *)
(*2.4 g g ->  t tbar g*)


proNo=8;
dir=FileNameJoin[{directory,"squaredAmp","8ggg"}];


sqBornFeynman=AmplitudeSquared[dir,amps[[proNo]],amps[[proNo]],"ReadSaved"->False,
	"Helicity"->All,"Parallelize"->True];
resFeynman=Get[sqBornFeynman]["Expression"]//GPDExplicit;


sqBornXi=AmplitudeSquared[dir,amps[[proNo]],amps[[proNo]],"ReadSaved"->False,
	"Helicity"->All,"Parallelize"->True,"GaugeXi"->"GaugeXi"[_]->\[Xi]];
resXi=Get[sqBornXi]["Expression"]//GPDExplicit;


(*different for individual diagram*)
diff=SimplifyList[resFeynman-resXi/.\[Xi]->1-\[Delta],\[Delta]]//CLTiming;


diff[[20]]


(*The same after summing over all diagrams*)
diffall=SimplifyList[Plus@@Plus@@diff,\[Delta]]//CLTiming


(* ::Subsection::Closed:: *)
(*2.5 squared amplitudes at loop level*)


dir=FileNameJoin[{directory,"squaredAmp","loops"}];


(*tree*tree for e+e- \[Rule] \[Mu]+ \[Mu]-*)
sqLoop0=AmplitudeSquared[dir,amps[[1]],amps[[1]],"ReadSaved"->False,"Helicity"->All,
	"ReferenceMomentum"->{}];
res0=Get[sqLoop0]["Expression"];


res0


(*1loop*tree for e+e- \[Rule] \[Mu]+ \[Mu]-*)
sqLoop1=AmplitudeSquared[dir,amps[[9]],amps[[1]],"ReadSaved"->False];
res1=Get[sqLoop1]["Expression"];


(*2loop*tree for e+e- \[Rule] \[Mu]+ \[Mu]-*)
sqLoop2=AmplitudeSquared[dir,amps[[10]],amps[[1]],"ReadSaved"->False,"Parallelize"->True];
res2=Get[sqLoop2]["Expression"];


res2//LeafCount


(*Other information*)
Delete[Get[sqLoop2],"Expression"]


(*1loop*1loop for gg\[Rule]HH*)
sqLoop11=AmplitudeSquared[dir,amps[[7]],amps[[7]],"ReadSaved"->False];
res11=Get[sqLoop11]["Expression"];


(*Other information*)
Delete[Get[sqLoop11],"Expression"]


(* ::Subsection::Closed:: *)
(*2.6 amplitudes at loop level*)


dir=FileNameJoin[{directory,"squaredAmp","loops"}];


(*1loop amplitude for gg\[Rule]HH*)
sqLoop1=AmplitudeSquared[dir,amps[[7]],1,"ReadSaved"->False];
res1=Get[sqLoop1]["Expression"];


res1//Variables


(* ::Section::Closed:: *)
(*4\:3001Loop integrals*)


(* ::Subsection::Closed:: *)
(*1 TensorDecomposition*)


LV[l1,\[Mu]]LV[l2,\[Nu]]//CLForm
TensorDecomposition[{l1,l2},{p},%]
% MT[\[Mu],\[Nu]]//CLForm//LorentzSimplify//Factor


TensorDecomposition[{l1},{p,q},LV[l1,\[Mu]]//CLForm]
% //CLForm//LorentzSimplify//Factor


TensorDecomposition[{l1},{p,q},LV[l1,\[Mu]]LV[l1,\[Nu]]//CLForm]
% LV[p,\[Nu]]//CLForm//LorentzSimplify//Factor


TensorDecomposition[{l1,l2},{p1,p2},3SP[l1,q1]+5SP[l2,q1]//CLForm]
%/.q1->p1//Simplify


TensorDecomposition[{l1,l2},{p1,p2},(3SP[l1,q1]+x^2 SP[l1,q2])^2//CLForm]
%/.q1->p1//Simplify


TensorDecomposition[{l1,l2},{p},SP[l1,p]^10000 SP[l2,q2]^2//CLForm]//CLTiming


Pair[Momentum[r1],Momentum[r1]]=0;
Pair[Momentum[r2],Momentum[r2]]=0;
TensorDecomposition[{l2},{r1,r2},SP[l2,q1]SP[l2,q2]//CLForm]//CLTiming
%/.q2->r1//Factor


Pair[Momentum[p1],Momentum[p1]]=0;
Pair[Momentum[p2],Momentum[p2]]=0;
TensorDecomposition[{l1,l2},{p1,p2},SP[l1,q1]^2 SP[l2,q2]SP[l2,q3]//CLForm]//CLTiming
%/.q2->p1//Factor


TensorDecomposition[{l1,l2,l3},{},SP[l1,q1]^10 SP[l3,q2]SP[l2,q3]^5//CLForm]//CLTiming


TensorDecomposition[{l1,l2},{p1,p2},LV[l1,mu]//CLForm]


Hold@TensorDecomposition[{l1,l2},{p1,p2},DiracChain[DiracSpinor[{p+q,1},m1],GS[l1]+m,DiracSpinor[{p-q,1},m2]]//CLForm]
%//ReleaseHold


Eps[mu1,mu2][l1,l2]//CLForm
TensorDecomposition[{l1,l2},{p1,p2},%]//EpsSimplify


(* ::Subsection::Closed:: *)
(*2 Feynman/Baikov Representation/ZeroSector*)


loopmom={l1,l2};
extmom={k1,k2,k3,k4};
pdlist={(l1)^2,(l1-k1)^2,(l1-k12)^2,(l2)^2,(l2-k123)^2,(l1-l2)^2,(l1-l2+k3)^2,(l2-k1)^2,(l2-k12)^2
		}/.{k12->k1+k2,k123->k1+k2+k3,k1234->k1+k2+k3+k4};
spsRep={k1^2->0,k2^2->0,k3^2->0,k4^2->0,k1*k2->s12/2,k2*k3->s23/2,k3*k4->s34/2,k4*k1->s41/2};
conservation=k1+k2+k3+k4->0;


{Bz,syzB}=BaikovPolynomial[z,pdlist,loopmom,spsRep,"ExternalMomenta"->extmom,"Relation"->conservation];
LeafCount/@{Bz,syzB}


Bz


{U,F}=SymanzikPolynomials[z,pdlist,loopmom,spsRep];
LeafCount/@{U,F}


loopmom={l1};
pdlist={(l1)^2,(l1+k1)^2};
spsRep={k1^2->0};
SymanzikPolynomials[z,pdlist,loopmom,spsRep]
ZeroSectorQ[pdlist,loopmom,spsRep]


loopmom={l1,l2};
pdlist={l1^2-m^2,l2^2-m^2,(p+l1+l2)^2-m^2};
spsRep={};
SymanzikPolynomials[z,pdlist,loopmom,spsRep]
ZeroSectorQ[pdlist,loopmom,spsRep]


loopmom={l1,l2};
pdlist={l1^2,(p+l1)^2,(l1+l2)^2,(p+l1+l2)^2,l2^2};
spsRep={};
SymanzikPolynomials[x,pdlist,loopmom,spsRep]//Factor
ZeroSectorQ[pdlist,loopmom,spsRep]


loopmom={l1,l2};
pdlist={l1^2,(p+l1)^2,l2^2};
spsRep={};
SymanzikPolynomials[x,pdlist,loopmom,spsRep]//Factor
ZeroSectorQ[pdlist,loopmom,spsRep]



RemoveZeroSector[GPD[{l1,l1,0,1},{l1+k,l1+k,0,1}],{l1},{k^2->0}]


RemoveZeroSector[GPD[{l1,l1,0,1},{l1,l2,0,1}],{l1,l2},{}]


RemoveZeroSector[GPD[{l1,l1,0,1},{l1,l2,-m^2,1},{p,p,-m^2,-1}],{l1,l2},{p*p->0}]


RemoveZeroSector[GPD[{l1,l1,0,1},{l1,l2,-m^2,1},{p,p,-m^2,-1}],{l1,l2},{p*p->m^2}]


(* ::Subsection::Closed:: *)
(*3 LoopSymmetry*)


LoopSymmetry[{{l,l,0},{l+p2,l+p2,0},{l-p1,l-p1,0}},{l},{}]


LoopSymmetry[{{l,l,0},{l+p1,l+p1,0},{l-p2,l-p2,0}},{l},{}]


LoopSymmetry[{{l1,l1,a},{p+l2,p+l2,b}},{l1,l2},{}]


LoopSymmetry[{{l1,l1,a},{p-l2,p-l2,b}},{l1,l2},{}]


LoopSymmetry[{{l,l,0},{l-p1,l-p1,0},{l-p1-p2,l-p1-p2,0}},{l},{}]


LoopSymmetry[{{l,l,0},{l-p1,l-p1,0},{l-p1+p2,l-p1+p2,0}},{l},{}]


LoopSymmetry[{{l,l,0},{l-p1,l-p1,0},{l-p1+p2,l-p1+p2,0}},{l},{},
	"CutInformation"->{{{p2-p1,p1}},{p1}}]


LoopSymmetry[{{l1,l1,-m^2,1},{l2,l2,-m^2,2},{-p+l1-l2,-p+l1-l2,-m^2,1}},
	{l1,l2},{}]
