(* ::Package:: *)

(* ::Chapter::Closed:: *)
(*\[Gamma]^*->\[Gamma]^**)


(*
Calculate the total cross section of e^+e^-->hadrons using optical theorem.
The main task is to calculate 2Im[Amp( \[Gamma]^*->\[Gamma]^* )].
*)


(* ::Subsubsection::Closed:: *)
(*init*)


$PrePrint=TraditionalForm;
directory=If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName;
Get[FileNameJoin[{directory,"..","..","calcloop","CalcLoop.wl"}]];


(*calculate the cross section with given Sqrt[s]*)
kinematics={s->100,$D->4-2eps};
(*define external momenta as p1, p2, ...*)
SetOptions[FeynArtsReadAmp,"ExternalMomentumName"->(Symbol["p"<>ToString@#]&)]
(*change p1^2 and p^2 from the default value 0 to s*)
SetOptions[AmplitudeSquared,"Offshellness"->{1->Sqrt[s],2->Sqrt[s]},"Replacement"->{CalcLoopSymbol["MU"]->0}]
SetOptions[AMFlowCalculateFI,"IBPReducer"->"Blade","Parallelize"->True,"Numeric"->kinematics]


(*replace polarization vectors of the two external photons by -g^\[Mu]\[Nu]*)
projection={#/.PolarizationVector[{p1,1},LorentzIndex["mu"[1]]] *PolarizationVector[{p2,-1},LorentzIndex["mu"[2]]]:>
	-Pair[LorentzIndex["mu"[1]],LorentzIndex["mu"[2]]]&,Identity,Identity};


ampSqDir[str_]:=FileNameJoin[{directory,"ampSquared",str}]
famDir[str_]:=FileNameJoin[{directory,"famDecomposed",str}]
intDir[str_]:=FileNameJoin[{directory,"loopIntegrated",str}]


ampDir=FileNames["*.FAs",directory,Infinity]//Sort
ampDir=Association@@(StringTake[FileBaseName@#,1;;-11]->#&/@ampDir);
ampDir//Keys


(* ::Subsubsection::Closed:: *)
(*Create amplitudes*)


processInf=Association[
	"Process"->"\[Gamma] -> \[Gamma] 1 0",
	"Options"-><|"InsertFields"->
		"{ExcludeParticles\[Rule]{_S*(_:1),V[1|2|3|4]*(_:1),U[1|2|3|4]*(_:1),\
							F[1|2|4,___]*(_:1),F[3,{2|3}]*(_:1)}}"|>
];
FeynArtsCreateAmp[FileNameJoin[{directory,"amplitudes","gamma1Loop"}],processInf]//CLTiming;


processInf=Association[
	"Process"->"\[Gamma] -> \[Gamma] 2 0",
	"Options"-><|"InsertFields"->
		"{ExcludeParticles\[Rule]{_S*(_:1),V[1|2|3|4]*(_:1),U[1|2|3|4]*(_:1),\
							F[1|2|4,___]*(_:1),F[3,{2|3}]*(_:1)}}"|>
];
FeynArtsCreateAmp[FileNameJoin[{directory,"amplitudes","gamma2Loop"}],processInf]//CLTiming;


processInf=Association[
	"Process"->"\[Gamma] -> \[Gamma] 3 0",
	"Options"-><|"InsertFields"->
		"{ExcludeParticles\[Rule]{_S*(_:1),V[1|2|3|4]*(_:1),U[1|2|3|4]*(_:1),\
							F[1|2|4,___]*(_:1),F[3,{2|3}]*(_:1)}}"|>
];
FeynArtsCreateAmp[FileNameJoin[{directory,"amplitudes","gamma3Loop"}],processInf]//CLTiming;


ampDir=FileNames["*.FAs",directory,Infinity]//Sort
ampDir=Association@@(StringTake[FileBaseName@#,1;;-11]->#&/@ampDir);
ampDir//Keys


(* ::Subsubsection::Closed:: *)
(*LO*)


current="gamma1Loop";


ampSq=AmplitudeSquared[ampSqDir@current,ampDir@current,1,"Operation"->projection];


families=FamilyDecomposition[famDir@current,ampSq,"CutMomenta"->{},"PhaseSpaceMomenta"->{}]//CLTiming;


resLO=AMFlowCalculateFI[intDir@current,families]//Expand//CLTiming;


(* ::Subsubsection::Closed:: *)
(*NLO*)


current="gamma2Loop";


ampSq=AmplitudeSquared[ampSqDir@current,ampDir@current,1,"Operation"->projection];


families=FamilyDecomposition[famDir@current,ampSq,"CutMomenta"->{},"PhaseSpaceMomenta"->{}]//CLTiming;


resNLO=AMFlowCalculateFI[intDir@current,families]//Expand//CLTiming;


(* ::Subsubsection::Closed:: *)
(*NNLO*)


current="gamma3Loop";


ampSq=AmplitudeSquared[ampSqDir@current,ampDir@current,1,"Operation"->projection];


families=FamilyDecomposition[famDir@current,ampSq,"CutMomenta"->{},"PhaseSpaceMomenta"->{}]//CLTiming;


resNNLO=AMFlowCalculateFI[intDir@current,families]//Expand//CLTiming;


(* ::Subsubsection::Closed:: *)
(*sum*)


loopfactor=(I \[Pi]^(2-eps))/(2\[Pi])^(4-2eps);
rep={SUNN->3,CalcLoopSymbol["GS"]->Sqrt[4\[Pi] \[Alpha]s],CalcLoopSymbol["EL"]->Sqrt[4\[Pi] \[Alpha]],CalcLoopSymbol["\[Epsilon]"]->eps};


(* amplitude from FeynArts is i*M *)
LO=loopfactor*resLO/I/.rep;
Series[LO,{eps,0,4}]//Normal//Expand//Rationalize
LO2=%/.{a_Real->0,Complex[a_,b_]:>2b}
(*(800 \[Alpha])/3*)


NLO=loopfactor^2*resNLO/I/.rep;
Series[NLO/LO2/(\[Alpha]s/\[Pi]),{eps,0,5}]//Normal//Expand//Rationalize
%/.{a_Real->0,Complex[a_,b_]:>2b}
(*1*)


NNLO=loopfactor^3*resNNLO/I/.rep;
Series[NNLO/LO2/(\[Alpha]s/\[Pi])^2,{eps,0,0}]//Normal//Expand//Rationalize
%/.{a_Real->0,Complex[a_,b_]:>2b}


Series[NNLO+NLO+LO/.\[Alpha]s->Z\[Alpha]s*\[Alpha]s*((\[Mu]^2E^EulerGamma)/(4\[Pi]))^ eps/.Z\[Alpha]s->1+\[Alpha]s/(4\[Pi]) (-11+2/3*1)/eps/.\[Mu]->10,
	{eps,0,0},{\[Alpha]s,0,2}]//Normal//Expand;
Expand[%/((800\[Alpha])/3)]/.{a_Real->0,Complex[a_,b_]:>2b}/.\[Alpha]s->"(\[Alpha]s/\[Pi])"*\[Pi]//Expand//Rationalize
(* 1+"(\[Alpha]s/\[Pi])"+1.87041200068419238420270274179527071974`14.836904766851545 ("(\[Alpha]s/\[Pi])")^2*)


(* ::Chapter::Closed:: *)
(*\[Gamma]^*->X*)


(*
Calculate the total cross section of e^+e^-->hadrons by combining various contributions.
*)


(* ::Subsubsection::Closed:: *)
(*init*)


$PrePrint=TraditionalForm;
directory=If[$FrontEnd===Null,$InputFileName,NotebookFileName[]]//DirectoryName;
Get[FileNameJoin[{directory,"..","..","calcloop","CalcLoop.wl"}]];


kinematics={s->100,$D->4-2eps};

SetOptions[FeynArtsReadAmp,"ExternalMomentumName"->(Symbol["p"<>ToString@#]&)]
SetOptions[AmplitudeSquared,"Offshellness"->{1->Sqrt[s]},"Replacement"->{CalcLoopSymbol["MU"]->0}]
SetOptions[AMFlowCalculateFI,"IBPReducer"->"Blade","Numeric"->kinematics]


ampSqDir[{str1_,str2_}]:=FileNameJoin[{directory,"ampSquared",str1<>"-"<>str2}]
famDir[{str1_,str2_}]:=FileNameJoin[{directory,"famDecomposed",str1<>"-"<>str2}]
intDir[{str1_,str2_}]:=FileNameJoin[{directory,"loopIntegrated",str1<>"-"<>str2}]


ampDir=FileNames["*.FAs",directory,Infinity]//Sort
ampDir=Association@@(StringTake[FileBaseName@#,1;;-11]->#&/@ampDir);
ampDir//Keys


(* ::Subsubsection::Closed:: *)
(*Create amplitudes*)


processes=InclusiveQCDProcesses["\[Gamma] -> u u~", 2, "Nf"->1]
processInf=Association[
	"Process"->#,
	"Options"-><|"InsertFields"->
		"{ExcludeParticles\[Rule]{_S*(_:1),V[1|2|3|4]*(_:1),U[1|2|3|4]*(_:1),\
							F[1|2|4,___]*(_:1),F[3,{2|3}]*(_:1)}}"|>
]&/@processes;


FeynArtsCreateAmp[FileNameJoin[{directory,"amplitudes",ToString@Hash@#@"Process"}],
	#]&/@processInf//CLTiming;


ampDir=FileNames["*.FAs",directory,Infinity]//Sort
ampDir=Association@@(StringTake[FileBaseName@#,1;;-11]->#&/@ampDir);
ampDir//Keys


(* ::Subsubsection::Closed:: *)
(*uu00*)


current=ToString/@Hash/@{"\[Gamma] -> u u~ 0 0","\[Gamma] -> u u~ 0 0"}


ampSq=AmplitudeSquared[ampSqDir@current,ampDir@current[[1]],ampDir@current[[2]]];


families=FamilyDecomposition[famDir@current,ampSq]//CLTiming;


resuu00=AMFlowCalculateFI[intDir@current,families]//Expand//CLTiming;


(* ::Subsubsection::Closed:: *)
(*uu10*)


current=ToString/@Hash/@{"\[Gamma] -> u u~ 1 0","\[Gamma] -> u u~ 0 0"}


ampSq=AmplitudeSquared[ampSqDir@current,ampDir@current[[1]],ampDir@current[[2]]];


families=FamilyDecomposition[famDir@current,ampSq]//CLTiming;


resuu10=AMFlowCalculateFI[intDir@current,families]//Expand//CLTiming;


(* ::Subsubsection::Closed:: *)
(*uug00*)


current=ToString/@Hash/@{"\[Gamma] -> u u~ g 0 0","\[Gamma] -> u u~ g 0 0"}


ampSq=AmplitudeSquared[ampSqDir@current,ampDir@current[[1]],ampDir@current[[2]]];


families=FamilyDecomposition[famDir@current,ampSq]//CLTiming;


resuug00=AMFlowCalculateFI[intDir@current,families]//Expand//CLTiming;


(* ::Subsubsection::Closed:: *)
(*uu11*)


current=ToString/@Hash/@{"\[Gamma] -> u u~ 1 0","\[Gamma] -> u u~ 1 0"}


ampSq=AmplitudeSquared[ampSqDir@current,ampDir@current[[1]],ampDir@current[[2]]];


families=FamilyDecomposition[famDir@current,ampSq]//CLTiming;


resuu11=AMFlowCalculateFI[intDir@current,families]//Expand//CLTiming;


(* ::Subsubsection::Closed:: *)
(*uu20*)


current=ToString/@Hash/@{"\[Gamma] -> u u~ 2 0","\[Gamma] -> u u~ 0 0"}


ampSq=AmplitudeSquared[ampSqDir@current,ampDir@current[[1]],ampDir@current[[2]]];


families=FamilyDecomposition[famDir@current,ampSq]//CLTiming;


resuu20=AMFlowCalculateFI[intDir@current,families]//Expand//CLTiming;


(* ::Subsubsection::Closed:: *)
(*uug10*)


current=ToString/@Hash/@{"\[Gamma] -> u u~ g 1 0","\[Gamma] -> u u~ g 0 0"}


ampSq=AmplitudeSquared[ampSqDir@current,ampDir@current[[1]],ampDir@current[[2]]];


families=FamilyDecomposition[famDir@current,ampSq]//CLTiming;


resuug10=AMFlowCalculateFI[intDir@current,families]//Expand//CLTiming;


(* ::Subsubsection::Closed:: *)
(*uugg00*)


current=ToString/@Hash/@{"\[Gamma] -> u u~ g g 0 0","\[Gamma] -> u u~ g g 0 0"}


ampSq=AmplitudeSquared[ampSqDir@current,ampDir@current[[1]],ampDir@current[[2]]];


families=FamilyDecomposition[famDir@current,ampSq]//CLTiming;


resuugg00=AMFlowCalculateFI[intDir@current,families]//Expand//CLTiming;


(* ::Subsubsection::Closed:: *)
(*uuGG00*)


current=ToString/@Hash/@{"\[Gamma] -> u u~ ug ug~ 0 0","\[Gamma] -> u u~ ug ug~ 0 0"}


ampSq=AmplitudeSquared[ampSqDir@current,ampDir@current[[1]],ampDir@current[[2]]];


families=FamilyDecomposition[famDir@current,ampSq]//CLTiming;


resuuUU00=AMFlowCalculateFI[intDir@current,families]//Expand//CLTiming;


(* ::Subsubsection::Closed:: *)
(*uuuu00*)


current=ToString/@Hash/@{"\[Gamma] -> u u~ u u~ 0 0","\[Gamma] -> u u~ u u~ 0 0"}


ampSq=AmplitudeSquared[ampSqDir@current,ampDir@current[[1]],ampDir@current[[2]]];


families=FamilyDecomposition[famDir@current,ampSq]//CLTiming;


resuuuu00=AMFlowCalculateFI[intDir@current,families]//Expand//CLTiming;


(* ::Subsubsection::Closed:: *)
(*sum*)


loopfactor=(I \[Pi]^(2-eps))/(2\[Pi])^(4-2eps);
rep={SUNN->3,CalcLoopSymbol["GS"]->Sqrt[4\[Pi] \[Alpha]s],CalcLoopSymbol["EL"]->Sqrt[4\[Pi] \[Alpha]],
	CalcLoopSymbol["\[Epsilon]"]->eps};


LO=resuu00/.rep;
LOexp=Series[LO,{eps,0,0}]//Normal//Rationalize
(*(800 \[Alpha])/3*)


NLO1=Expand@Normal@Series[2*loopfactor*resuu10/.rep,{eps,0,1}]/.{Complex[a_,b_]:>a}
NLO2=Expand@Normal@Series[resuug00/.rep,{eps,0,1}]
NLO=NLO1+NLO2//Chop
Expand@Normal@Series[NLO/LO/(\[Alpha]s/\[Pi]),{eps,0,0}]//Rationalize
(*1*)


NNLO1=Expand@Normal@Series[2*loopfactor^2*resuu20/.rep,{eps,0,0}]/.{Complex[a_,b_]:>a};
NNLO2=Expand@Normal@Series[-loopfactor^2*resuu11/.rep,{eps,0,0}];
NNLO3=Expand@Normal@Series[2loopfactor*resuug10/.rep,{eps,0,0}]/.{Complex[a_,b_]:>a};
NNLO4=Expand@Normal@Series[resuugg00/.rep,{eps,0,0}];
NNLO5=Expand@Normal@Series[resuuUU00/.rep,{eps,0,0}];
NNLO6=Expand@Normal@Series[ resuuuu00/.rep,{eps,0,0}];
NNLO=Plus@@{NNLO1,NNLO2,NNLO3,NNLO4,NNLO5,NNLO6}//Chop
Expand@Normal@Series[NNLO/LO,{eps,0,0}]


Series[(NNLO+NLO+LO)/LOexp/.\[Alpha]s->Z\[Alpha]s*\[Alpha]s*((\[Mu]^2E^EulerGamma)/(4\[Pi]))^eps/.Z\[Alpha]s->1+\[Alpha]s/(4\[Pi]) (-11/3*3+2/3*1)/eps/.\[Mu]->10,
	{eps,0,0},{\[Alpha]s,0,2}]//Normal
%/.\[Alpha]s->"(\[Alpha]s/\[Pi])"*\[Pi]//Expand//Rationalize


FindIntegerNullVector[N[{1.8704120006841923842027056643,1,Zeta[3]},13]]


(*arXiv:9606230*)
res={1,CF*\[CapitalPi]1,CF^2 \[CapitalPi]A+CF*CA*\[CapitalPi]NA+CF*T*nl*\[CapitalPi]l+CF*T*\[CapitalPi]F}/.\[CapitalPi]l->\[CapitalPi]F/.
	{\[CapitalPi]1->3/4,\[CapitalPi]A->-3/32,\[CapitalPi]NA->123/32-11/4*Zeta[3]+11/16*log,\[CapitalPi]F->-11/8+Zeta[3]-1/4*log};
(*QED*)QEDrep={CF->1,CA->0,T->1,nl->0}
(*QCD*)QCDrep={CF->4/3,CA->3,T->1/2,nl->0}
res/.QCDrep//Expand
%//N
res/.QEDrep//Expand
%//N
